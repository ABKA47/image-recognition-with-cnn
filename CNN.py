import tensorflow as tf
import numpy as np
from keras.preprocessing import image
from keras.preprocessing.image import ImageDataGenerator

###### PART 1 DATA PREPROCESSING ######
##### PREPROCESSING THE TRAINNING SET #####

train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True
)
training_set = train_datagen.flow_from_directory(
    'dataset/training_set',
    batch_size=32,
    target_size=(64, 64),
    class_mode='binary')

##### PREPROCESSING THE TEST SET #####

test_datagen = ImageDataGenerator(rescale=1. / 255)
test_set = test_datagen.flow_from_directory(
    'dataset/test_set',
    batch_size=32,
    target_size=(64, 64),
    class_mode='binary'
)

###### PART 2 BUILDING THE CNN ######
##### INITIALISING THE CNN #####

cnn = tf.keras.models.Sequential()

## STEP 1 CONVOLUTION ##

cnn.add(tf.keras.layers.Conv2D(filters=32, kernel_size=3, activation='relu', input_shape=[64, 64, 3]))

## STEP 2 POOLING ##
cnn.add(tf.keras.layers.MaxPool2D(pool_size=2, strides=2))

## ADDING A SECOND CONVOLUTIONAL LAYER ##

cnn.add(tf.keras.layers.Conv2D(filters=32, kernel_size=3, activation='relu'))
cnn.add(tf.keras.layers.MaxPool2D(pool_size=2, strides=2))

## STEP 3 FLATTING ##

cnn.add(tf.keras.layers.Flatten())

## STEP 4 FULL CONNECTION ##

cnn.add(tf.keras.layers.Dense(units=128, activation='relu'))

## STEP 5 OUTPUT LAYER ##

cnn.add(tf.keras.layers.Dense(units=1, activation='sigmoid'))

###### PART 3 TRAINING THE CNN ######
## COMPILING THE CNN ##

cnn.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

## TRAINING THE CNN ON THE TRAINING SET AND EVOLUATING THE IT ON THE TEST SET ##

cnn.fit(x=training_set, validation_data=test_set, epochs=25)

###### PART 4 MAKING A SINGLE PREDICTION ######
test_image = image.load_img('dataset/single_prediction/cat_or_dog_1.jpg', target_size=(64, 64))
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis=0)
result = cnn.predict(test_image)
if result[0][0] > 0.5:
    prediction = 'dog'
else:
    prediction = 'cat'

print(prediction)
